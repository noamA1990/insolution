// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyD7jrxV5zBh2TKGjhWsPDnxsPo8p-vbpi4",
    authDomain: "insolution-fbec7.firebaseapp.com",
    databaseURL: "https://insolution-fbec7.firebaseio.com",
    projectId: "insolution-fbec7",
    storageBucket: "insolution-fbec7.appspot.com",
    messagingSenderId: "940233005919",
    appId: "1:940233005919:web:12d4f8ddf0df51d68034ea"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
