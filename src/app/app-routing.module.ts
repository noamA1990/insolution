import { SearchComponent } from './general Components/search/search.component';
import { NewsComponent } from './general Components/news/news.component';
import { CheckdataComponent } from './general Components/checkdata/checkdata.component';
import { ProfileComponent } from './profile Components/profile/profile.component';
import { SignInComponent } from './logIn System Components/sign-in/sign-in.component';
import { Routes, RouterModule } from '@angular/router';
import { SignUpComponent } from './logIn System Components/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './logIn System Components/forgot-password/forgot-password.component';
import { NgModule } from '@angular/core';
import { SecureInnerPagesGuard } from './shared/gurad/secure-inner-pages.guard';
import { AuthGuard } from './shared/gurad/auth.guard';
import { WelcomeComponent } from './general Components/welcome/welcome.component';
import { DashboardComponent } from './dashboard Components/dashboard/dashboard.component';
import { ErrorPageComponent } from './general Components/error-page/error-page.component';
import { VerifyEmailComponent } from './logIn System Components/verify-email/verify-email.component';

const appRoutes: Routes = [
    { path: '', redirectTo: '/welcome', pathMatch: 'full' },
    { path: 'welcome', component: WelcomeComponent, canActivate: [SecureInnerPagesGuard] },
    { path: 'authentication', children: [
        { path: 'log-in', component: SignInComponent, canActivate: [SecureInnerPagesGuard] },
        { path: 'register-user', component: SignUpComponent, canActivate: [SecureInnerPagesGuard] },
        { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [SecureInnerPagesGuard] },
        { path: 'verify-email-address', component: VerifyEmailComponent, canActivate: [SecureInnerPagesGuard] },
    ] },
    
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'search', component: SearchComponent, canActivate: [AuthGuard] },
    { path: 'news', component: NewsComponent, canActivate: [AuthGuard] },
    { path: 'profile', children: [
            { path: '', component: ProfileComponent, canActivate: [AuthGuard] },
        ] },
    { path: 'insoCheck', component: CheckdataComponent, canActivate: [AuthGuard] },
    { path: 'not-found', component: ErrorPageComponent },
    { path: '**', redirectTo: '/not-found' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
})

export class AppRoutingModule { }