import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit {

  @Output() closeSidenav = new EventEmitter<void>();

  links = [
    { text: 'Profile', ref: '/profile' },
    { text: 'Check Data', ref: '/insoCheck' },
    { text: 'Search Pictures', ref: '/search'},
    { text: 'News', ref: '/news'},
    { text: 'Logout', ref: '' },
  ];

  constructor(public authService: AuthService) { }

  ngOnInit() { }

  onClose() {
    this.closeSidenav.emit();
  }

  SignOut(){
    this.authService.SignOut();
  }
}
