import { Observable } from 'rxjs';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  url = true;
  @Output() sidenavToggle = new EventEmitter<void>();
  userName: string;
  uid: string;
  user$: Observable<any>;

  systemLinks = [
    { text: 'Home', ref: '/dashboard' },
    { text: 'Check Data', ref: '/insoCheck' },
    { text: 'Search Pictures', ref: '/search'},
    { text: 'News', ref: '/news'}
  ];
  userLink = [
    { text: 'Profile', ref: '/profile' },
    { text: 'Logout', ref: '' }
  ]; 

  constructor(public auth: AuthService, private profileservice: ProfileService) { }

  ngOnInit() {
    if (this.auth.isLoggedIn) {
      this.uid = this.auth.getUser().uid;
      this.user$ = this.profileservice.findUser(this.uid);
      this.user$.subscribe(data => {
        this.userName = data.data().firstName;
      })
    }
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }

  SignOut() {
    this.auth.SignOut();
  }
}
