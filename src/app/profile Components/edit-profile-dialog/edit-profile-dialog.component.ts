import { Observable } from 'rxjs';
import { Component, OnInit, Inject } from '@angular/core';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EditDialogComponent } from '../../dashboard Components/edit-dialog/edit-dialog.component';
import { FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-edit-profile-dialog',
  templateUrl: './edit-profile-dialog.component.html',
  styleUrls: ['./edit-profile-dialog.component.css']
})
export class EditProfileDialogComponent implements OnInit {

  firstName: string;
  lastName: string;
  age: number;
  gender: string;
  uid: string;
  genders = ['male', 'female'];
  birth: Date;
  minDate: Date;
  maxDate: Date;
  userProfile$: Observable<any>;

  constructor(private proservice: ProfileService, public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 100, 0, 1);
    this.maxDate = new Date(currentYear - 1, 0, 1);
  }

  firstNameFormControl = new FormControl('', [
    Validators.required,

  ]);
  lastNameFormControl = new FormControl('', [
    Validators.required,

  ]);
  birthFormControl = new FormControl('', [
    Validators.required,
  ]);
  matcher = new MyErrorStateMatcher();

  ngOnInit() {
    this.userProfile$ = this.proservice.findUser(this.data);
    this.userProfile$.subscribe(user => {
      this.firstName = user.data().firstName;
      this.lastName = user.data().lastName;
      this.birth = new Date(user.data().birthDay.toDate());
      this.gender = user.data().gender;
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onSubmit() {
    this.dialogRef.close({ first: this.firstName, last: this.lastName, userBirth: this.birth, userGender: this.gender });
  }
}
