import { ProfileService } from '../../shared/services/profile.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { EditProfileDialogComponent } from '../edit-profile-dialog/edit-profile-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import * as moment from 'moment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  firstName: string;
  lastName: string;
  age: number;
  email: string;
  gender: string;
  uid: string;
  password: string;
  genders = ['male', 'female'];
  userProfile$;
  birth: Date;

  constructor(private auth:AuthService, public dialog: MatDialog, private profileservice: ProfileService) { }

  ngOnInit() {
    this.uid = this.auth.getUser().uid;
    this.userProfile$ = this.profileservice.findUser(this.uid);
    this.userProfile$.subscribe(user =>{
      this.firstName = user.data().firstName;
      this.lastName = user.data().lastName;
      this.birth = new Date(user.data().birthDay.toDate());
      this.gender = user.data().gender;
      this.email = user.data().email;
      this.age = this.ageFormDateOfBirth(this.birth);
    })
  }

  ageFormDateOfBirth(dateOfBirth): number {
    return (moment().diff(dateOfBirth, 'months')/12)
  }
  openDialog(data): void {
    const dialogRef = this.dialog.open(EditProfileDialogComponent, {
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result !== undefined){
        this.firstName = result.first;
        this.lastName = result.last;
        this.birth = result.userBirth;
        this.gender = result.userGender;
        this.age = this.ageFormDateOfBirth(this.birth);
        this.updateProfile();
      }
    });
  }
  updateProfile(){
      this.profileservice.updateProfile(this.uid, this.firstName, this.lastName, this.birth, this.gender);
  }
}