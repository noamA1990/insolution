import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Insolution';
  
  url = false;
  constructor(private router: Router, location: Location) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd ) {
        
        if (event.url === '/not-found' || location.path().endsWith('not-found')) {
          this.url = false;
        } else {
          this.url = true;
        }
      }
    });
  }
}
