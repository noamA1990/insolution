import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SignInComponent } from './logIn System Components/sign-in/sign-in.component';
import { SignUpComponent } from './logIn System Components/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './logIn System Components/forgot-password/forgot-password.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './shared/services/auth.service';
import { HeaderComponent } from './nav/header/header.component';
import { SidenavListComponent } from './nav/sidenav-list/sidenav-list.component';
import { AppRoutingModule } from './app-routing.module';
import { environment } from 'src/environments/environment';
import { DashboardComponent, DeleteDialog } from './dashboard Components/dashboard/dashboard.component';
import { ErrorPageComponent } from './general Components/error-page/error-page.component';
import { WelcomeComponent } from './general Components/welcome/welcome.component';
import { ProfileComponent } from './profile Components/profile/profile.component';
import { CheckdataComponent } from './general Components/checkdata/checkdata.component';
import { EditDialogComponent } from './dashboard Components/edit-dialog/edit-dialog.component';
import { EditProfileDialogComponent } from './profile Components/edit-profile-dialog/edit-profile-dialog.component';
import { SearchComponent } from './general Components/search/search.component';
import { NewsComponent } from './general Components/news/news.component';
import { VerifyEmailComponent } from './logIn System Components/verify-email/verify-email.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    HeaderComponent,
    SidenavListComponent,
    DashboardComponent,
    ErrorPageComponent,
    WelcomeComponent,
    ProfileComponent,
    CheckdataComponent,
    EditDialogComponent,
    EditProfileDialogComponent,
    DeleteDialog,
    SearchComponent,
    NewsComponent,
    VerifyEmailComponent,
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    HttpClientModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  providers: [AuthService],
  bootstrap: [AppComponent],
  entryComponents: [EditDialogComponent, EditProfileDialogComponent, DeleteDialog]
})
export class AppModule { }
