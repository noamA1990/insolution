export interface User {
    uid: string;
    email: string;
    firstName: string;
    lastName: string;
    birthDay: Date;
    gender: string;
    emailVerified: boolean;
}