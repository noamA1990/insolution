import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(public snackBar: MatSnackBar, private router: Router) { }

  action = "Back Home";
  config: MatSnackBarConfig = {
    duration: 10000,
    horizontalPosition: 'center',
    verticalPosition: 'top',
    
  };


  info(msg) {
    this.config['panelClass'] = ['notification', 'info'];
    let snackBarRef = this.snackBar.open(msg, this.action, this.config);
    snackBarRef.afterDismissed().subscribe(action =>{
      if(action){
        this.router.navigate(['/dashboard'])
      }
    })
  }
}
