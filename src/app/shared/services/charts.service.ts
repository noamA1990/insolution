import { Injectable } from '@angular/core';
import { Chart } from 'node_modules/chart.js'

@Injectable({
  providedIn: 'root'
})
export class ChartsService {

  constructor() { }

  createBmiChat(xArray, yArray) {
    new Chart("bmiChart", {
      type: 'line',
      data: {
        labels: xArray,
        datasets: [{
          label: 'Bmi Chart',
          data: yArray,
          fill: false,
          pointBorderColor: 'rgba(102, 102, 255, 1)',
          pointRadius: 4,
          borderColor: [
            'rgba(102, 102, 255, 1)',
          ],
          borderWidth: 1
        }]
      },
      options: {
        legend: {
          labels: {
            fontColor: "white",
            fontSize: 18
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'rgba(255, 255, 255, 1)'
            }
          }],
          xAxes: [{
            ticks: {
              fontColor: 'rgba(255, 255, 255, 1)'
            }
          }]
        },
      }
    });
  }

  createBloodPressureChat(xArray, yArray) {
    new Chart("bloodPressure", {
      type: 'line',
      data: {
        labels: xArray,
        datasets: [{
          label: 'Blood Pressure Chart',
          data: yArray,
          fill: false,
          pointBorderColor: 'rgba(51, 255, 51, 1)',
          pointRadius: 4,
          borderColor: [
            'rgba(51, 255, 51, 1)',
          ],
          borderWidth: 1
        }]
      },
      options: {
        legend: {
          labels: {
            fontColor: "white",
            fontSize: 18
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              fontColor: 'rgba(255, 255, 255, 1)'
            }
          }],
          xAxes: [{
            ticks: {
              fontColor: 'rgba(255, 255, 255, 1)'
            }
          }]
        },
      }
    });
  }
}
