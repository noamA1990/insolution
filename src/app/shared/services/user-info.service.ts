import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {

  constructor(private db: AngularFirestore, private http: HttpClient) { }

  usersCollection: AngularFirestoreCollection = this.db.collection('users');
  infoCollection: AngularFirestoreCollection;
  private URL = "https://oe3dnow7f2.execute-api.us-east-1.amazonaws.com/beta";

  public checkBmi: number;
  public checkPreg: number;
  public checkGlucose: number;
  public checkDiastolic: number;
  public checkAge: number;
  public checkTriceps: number;
  public checkInsulin: number;
  public checkDiabetes: number;

  addInfo(userId: string, bmi: number, preg_count: number, glucose: number, diastolic: number,
    triceps: number, insulin: number, diabetes: number, height: number, weight: number, create) {
    this.usersCollection.doc(userId).collection('info').add({
      bmi: bmi,
      preg_count: preg_count,
      glucose: glucose,
      diastolic: diastolic,
      triceps: triceps,
      insulin: insulin,
      diabetes: diabetes,
      weight: weight,
      height: height,
      createdAt: create
    })
  }

  getInfo(userId: string): Observable<any> {
    this.infoCollection = this.db.collection(`users/${userId}/info`);
    return this.infoCollection.snapshotChanges().pipe(
      map(collection => collection.map(document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data;
      }))
    );
  }

  getClassified(userId: string): Observable<any> {
    this.infoCollection = this.db.collection(`users/${userId}/classified`);
    return this.infoCollection.snapshotChanges().pipe(
      map(collection => collection.map(document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data;
      }))
    );
  }
  addClassified(result:any, userId: string){
    this.usersCollection.doc(userId).collection('classified').add({
      classification: result,
      createdAt: new Date()
    })
  }
  updateInfo(infoId: string, userId: string, bmi: number, preg_count: number, glucose: number, diastolic: number,
    triceps: number, insulin: number, diabetes: number, height: number, weight: number, create) {
    this.db.doc(`users/${userId}/info/${infoId}`).update(
      {
        bmi: bmi,
        preg_count: preg_count,
        glucose: glucose,
        diastolic: diastolic,
        triceps: triceps,
        insulin: insulin,
        diabetes: diabetes,
        weight: weight,
        height: height,
        createdAt: create
      })
  }
  deleteInfo(userId: string, key: string){
    this.db.doc(`users/${userId}/info/${key}`).delete();
  }

  classify(): Observable<any> {
    let json = {
      "instances": [
        {
          "features": [
            this.checkPreg,
            this.checkGlucose,
            this.checkTriceps,
            this.checkDiastolic,
            this.checkInsulin,
            this.checkBmi,
            this.checkDiabetes,
            this.checkAge,
          ]
        }
      ]
    }

    return this.http.post<any>(this.URL, json).pipe(
      map(res => {
        let final = res.body.replace('[', '');
        final = final.replace(']', '');
        return final;
      })
    );
  }
}