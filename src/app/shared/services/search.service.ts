import { Image } from '../interfaces/image';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Article } from '../interfaces/article';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private URL = 'https://pixabay.com/api/?key=16271964-fd3899799274c5aec249b8df0&q=';
  private ArticlesURL = 'https://newsapi.org/v2/top-headlines?country=us&category=';
  private pageSize = 30;
  private KEY = 'e1bb96e84b464b0693c5f3d71720b23a';

  constructor(private http: HttpClient) { }

  getSearchResults(q: string): Observable<Image> {
    return this.http.get<Image>(`${this.URL}${q}&image_type=photo&pretty=true&per_page=100`);
  }

  getArticles(category: string): Observable<Article> {
    return this.http.get<Article>(`${this.ArticlesURL}${category}&pageSize=${this.pageSize}&apiKey=${this.KEY}`)
  }
}
