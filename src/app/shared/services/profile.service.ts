import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private db: AngularFirestore) { }

  findUser(key: string) {
    return this.db.collection('users').doc(key).get();
  }

  updateProfile(key: string, firstname: string, lastname: string, birthDay: Date, gender: string) {
    this.db.collection('users').doc(key).update({
      firstName: firstname,
      lastName: lastname,
      birthDay: birthDay,
      gender: gender
    })
  }
}
