import { Article } from '../../shared/interfaces/article';
import { Observable } from 'rxjs';
import { SearchService } from '../../shared/services/search.service';
import { Component, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material/core';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  links = [ {text: 'Health', value: 'health'}, 
            {text: 'Science', value: 'science'}, 
            {text: 'Sports', value: 'sports'}];
  
  activeLink = this.links[0].text;
  background: ThemePalette = undefined;
  articles$: Observable<Article>;
  articlesArray =[];
  panelOpenState = false;
  pageSize = 15;
  page = 1;
  constructor(private searchservice: SearchService) { }

  ngOnInit() {
    this.articles$ = this.searchservice.getArticles(this.activeLink);
    this.articles$.subscribe(data => {
      this.articlesArray = data.articles;
    })
  }

  slected(){
    this.articles$ = this.searchservice.getArticles(this.activeLink);
    this.articles$.subscribe(data => {
      this.articlesArray = data.articles;
    })
  }

}
