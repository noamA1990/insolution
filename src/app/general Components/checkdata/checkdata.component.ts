import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserInfoService } from '../../shared/services/user-info.service';
import { ProfileService } from '../../shared/services/profile.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import * as moment from 'moment';
import { NotificationService } from 'src/app/shared/services/notification.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-checkdata',
  templateUrl: './checkdata.component.html',
  styleUrls: ['./checkdata.component.css']
})
export class CheckdataComponent implements OnInit {

  user$: Observable<any>;
  userId: string;
  gender: string;
  age: number;
  preg_count: number;
  glucose: number;
  diastolic: number;
  triceps: number;
  insulin: number;
  diabetes: number;
  weight: number;
  height: number;
  bmi: number;
  examinationDate: Date = new Date();
  birth: Date;
  tricepsMin:number;
  tricepsMax: number;

  minDate: Date;
  maxDate: Date;

  matcher = new MyErrorStateMatcher();

  pregFormControl = new FormControl('', [
    Validators.required,
    Validators.max(15)
  ]);

  glucoseFormControl = new FormControl('', [
    Validators.required,
    Validators.min(70),
    Validators.max(110)
  ]);
  diastolicFormControl = new FormControl('', [
    Validators.required,
    Validators.min(60),
    Validators.max(95)
  ]);
  tricepsFormControl = new FormControl('', [
    Validators.required,
    Validators.min(91),
    Validators.max(120)
  ]);
  insulinFormControl = new FormControl('', [
    Validators.required,
    Validators.min(16),
    Validators.max(175)
  ]);
  diabetesFormControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
    Validators.max(2.4)
  ]);
  weightFormControl = new FormControl('', [
    Validators.required,
    Validators.max(300),
    Validators.maxLength(3),
    Validators.min(2.5),
  ]);
  heightFormControl = new FormControl('', [
    Validators.required,
    Validators.max(3),
    Validators.min(0.5),
  ]);
  examinationFormControl = new FormControl('', [
    Validators.required,
  ]);

  constructor(private auth: AuthService, private proservice: ProfileService, 
              private userInfoservice: UserInfoService, private router: Router, 
              private notificationService: NotificationService)
  {
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 1, 0, 1);
    this.maxDate = new Date();
  }

  ageFormDateOfBirth(dateOfBirth): number {
    return (moment().diff(dateOfBirth, 'months')/12)
  }
  ngOnInit() {
    this.userId = this.auth.getUser().uid;
    this.user$ = this.proservice.findUser(this.userId);
    this.user$.subscribe(user => {
      this.birth = new Date(user.data().birthDay.toDate());
      this.gender = user.data().gender;
      this.age = this.ageFormDateOfBirth(this.birth);
      if(this.gender === 'male'){
        this.tricepsMin = 91;
        this.tricepsMax = 100;
      }
      else{
        this.tricepsMin = 101;
        this.tricepsMax = 120;
      }
    })
  }

  check() {
    if(this.gender === 'male'){
      this.preg_count = 0;
    }
    if (this.pregFormControl.getError('max') || this.weightFormControl.getError('max') ||
      this.weightFormControl.getError('min') || this.heightFormControl.getError('max') ||
      this.heightFormControl.getError('min') || this.glucoseFormControl.getError('max') ||
      this.glucoseFormControl.getError('min') || this.insulinFormControl.getError('max') ||
      this.insulinFormControl.getError('min') || this.tricepsFormControl.getError('max') ||
      this.tricepsFormControl.getError('min') || this.diastolicFormControl.getError('max') ||
      this.diastolicFormControl.getError('min') || this.diabetesFormControl.getError('max') ||
      this.diabetesFormControl.getError('min')) {
      return
    }
    if (this.preg_count !== undefined && this.glucose !== undefined && this.insulin !== undefined
      && this.triceps !== undefined && this.diabetes !== undefined && this.diastolic !== undefined
      && this.weight !== undefined && this.height !== undefined) {

      this.bmi = (this.weight / (this.height * this.height));

      if (this.gender === 'male') {
        this.userInfoservice.addInfo(this.userId, this.bmi, 0, this.glucose, this.diastolic,
          this.triceps, this.insulin, this.diabetes, this.height, this.weight, this.examinationDate);
        this.userInfoservice.checkAge = this.age;
        this.userInfoservice.checkBmi = this.bmi;
        this.userInfoservice.checkDiabetes = this.diabetes;
        this.userInfoservice.checkDiastolic = this.diastolic;
        this.userInfoservice.checkGlucose = this.glucose;
        this.userInfoservice.checkInsulin = this.insulin;
        this.userInfoservice.checkPreg = 0;
        this.userInfoservice.checkTriceps = this.triceps;
      }
      else {
        this.userInfoservice.addInfo(this.userId, this.bmi, this.preg_count, this.glucose, this.diastolic,
          this.triceps, this.insulin, this.diabetes, this.height, this.weight, this.examinationDate);
        this.userInfoservice.checkAge = this.age;
        this.userInfoservice.checkBmi = this.bmi;
        this.userInfoservice.checkDiabetes = this.diabetes;
        this.userInfoservice.checkDiastolic = this.diastolic;
        this.userInfoservice.checkGlucose = this.glucose;
        this.userInfoservice.checkInsulin = this.insulin;
        this.userInfoservice.checkPreg = this.preg_count;
        this.userInfoservice.checkTriceps = this.triceps;
      }
      this.notificationService.info('The informtion saved successfully')
    }
    
  }
}
