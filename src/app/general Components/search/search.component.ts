import { Observable } from 'rxjs';
import { SearchService } from '../../shared/services/search.service';
import { Component, OnInit } from '@angular/core';
import { Image } from 'src/app/shared/interfaces/image';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchItem: string;
  images$: Observable<Image>;
  array = [];
  searchFlag;
  
  searchFormControl = new FormControl('', [])

  options: string[] = [
    'Orange',
    'Apple',
    'Basketball',
    'Football',
    'Meat',
    'Healthy Drinks',
    'Energy Bar',
    'Blood',
    'Fitness',
    'Insulin',
    'Glucose',
    'Sport',
    'View',
    'Beaches',
    'Blood Pressure',
    'Yogurt',
    'Watermelon',
    'Melon',
    'Vegetables',
    'Vegan Dishes',
    'Vegetarianism',
    'Healthy Food',
    'Frozen Yogurt',
    'Green Apple',
    'Red Apple',
    'Banna',
    'Fruits',
  ];
  searchOptions = [
    'Orange',
    'Apple',
    'Healthy Drinks',
    'Glucose',
    'Yogurt',
    'Healthy Food',
  ];

  filteredOptions: Observable<string[]>;
  page = 1;
  pageSize = 15;
  constructor(private searchservice: SearchService) { }

  ngOnInit() {
    this.searchItem;
    this.filteredOptions = this.searchFormControl.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  search(){
    if(this.searchItem !== undefined){
      this.searchFlag = false;
      this.images$ = this.searchservice.getSearchResults(this.searchItem);
      this.images$.subscribe(data => {
      this.array = data.hits;
    })
    }
    else{
      this.searchFlag = true;
    }
    
  }


  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
}
