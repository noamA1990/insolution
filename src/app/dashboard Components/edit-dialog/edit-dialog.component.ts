import { Observable } from 'rxjs';
import { ProfileService } from '../../shared/services/profile.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserInfoService } from 'src/app/shared/services/user-info.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher, ThemePalette } from '@angular/material/core';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.css']
})
export class EditDialogComponent implements OnInit {

  user$: Observable<any>;
  userId: string;
  gender: string;
  age: number;
  preg_count: number;
  glucose: number;
  diastolic: number;
  triceps: number;
  insulin: number;
  diabetes: number;
  weight: number;
  height: number;
  bmi: number;
  examinationDate: Date;
  infoKey: string;
  tricepsMin:number;
  tricepsMax: number;

  minDate: Date;
  maxDate: Date;

  matcher = new MyErrorStateMatcher();

  pregFormControl = new FormControl('', [
    Validators.required,
    Validators.max(12)
  ]);

  glucoseFormControl = new FormControl('', [
    Validators.required,
    Validators.min(70),
    Validators.max(100)
  ]);
  diastolicFormControl = new FormControl('', [
    Validators.required,
    Validators.min(60),
    Validators.max(80)
  ]);
  tricepsFormControl = new FormControl('', [
    Validators.required,
    Validators.min(91),
    Validators.max(120)
  ]);
  insulinFormControl = new FormControl('', [
    Validators.required,
    Validators.min(16),
    Validators.max(166)
  ]);
  diabetesFormControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
    Validators.max(2.4)
  ]);
  weightFormControl = new FormControl('', [
    Validators.required,
    Validators.max(300),
    Validators.maxLength(3),
    Validators.min(2.5),
  ]);
  heightFormControl = new FormControl('', [
    Validators.required,
    Validators.max(3),
    Validators.min(0.5),
  ]);
  examinationFormControl = new FormControl('', [
    Validators.required,

  ]);

  constructor(private userinfoservice: UserInfoService, private auth: AuthService,
    private proservice: ProfileService, public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
      const currentYear = new Date().getFullYear();
      this.minDate = new Date(currentYear - 1, 0, 1);
      this.maxDate = new Date();
    }

  ngOnInit() {
    this.userId = this.auth.getUser().uid;
    this.user$ = this.proservice.findUser(this.userId);
    this.user$.subscribe(user => {
      this.age = user.data().age;
      this.gender = user.data().gender;
      if(this.gender === 'male'){
        this.tricepsMin = 91;
        this.tricepsMax = 100;
      }
      else{
        this.tricepsMin = 101;
        this.tricepsMax = 120;
      }
    })
    this.glucose = this.data.glucose;
    this.preg_count = this.data.preg_count;
    this.diabetes = this.data.diabetes;
    this.diastolic = this.data.diastolic;
    this.insulin = this.data.insulin;
    this.triceps = this.data.triceps;
    this.weight = this.data.weight;
    this.height = this.data.height;
    this.examinationDate = this.data.createdAt.toDate();
    this.infoKey = this.data.id;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  update() {
    if (this.pregFormControl.getError('max') || this.weightFormControl.getError('max') ||
      this.weightFormControl.getError('min') || this.heightFormControl.getError('max') ||
      this.heightFormControl.getError('min') || this.glucoseFormControl.getError('max') ||
      this.glucoseFormControl.getError('min') || this.insulinFormControl.getError('max') ||
      this.insulinFormControl.getError('min') || this.tricepsFormControl.getError('max') ||
      this.tricepsFormControl.getError('min') || this.diastolicFormControl.getError('max') ||
      this.diastolicFormControl.getError('min') || this.diabetesFormControl.getError('max') ||
      this.diabetesFormControl.getError('min')) {
      return
    }
    if (this.preg_count !== undefined && this.glucose !== undefined && this.insulin !== undefined
      && this.triceps !== undefined && this.diabetes !== undefined && this.diastolic !== undefined
      && this.weight !== undefined && this.height !== undefined) {

      this.bmi = (this.weight / (this.height * this.height));
      if (this.gender === 'male') {
        this.userinfoservice.updateInfo(this.infoKey, this.userId, this.bmi, 0, this.glucose, this.diastolic,
          this.triceps, this.insulin, this.diabetes, this.height, this.weight, this.examinationDate);
          this.dialogRef.close();
      }
      else {
        this.userinfoservice.updateInfo(this.infoKey, this.userId, this.bmi, this.preg_count, this.glucose, 
          this.diastolic, this.triceps, this.insulin, this.diabetes, this.height, this.weight, this.examinationDate);
          this.dialogRef.close();
      }
    }
  }
}
