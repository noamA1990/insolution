import { ChartsService } from '../../shared/services/charts.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { UserInfoService } from '../../shared/services/user-info.service';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EditDialogComponent } from '../edit-dialog/edit-dialog.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})

export class DashboardComponent implements OnInit {

  displayedColumns: string[] = ['createdAt', 'weight', 'insulin', 'glucose', 'diastolic', 'action'];

  uid: string;
  info$: Observable<any[]>;
  dataSource;
  pipe = new DatePipe('en-US');
  length;
  flag = false;
  infoKey: string;
  lastResult: number;
  classifiedResults$: Observable<any[]>;
  message: string;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(private userinfoservice: UserInfoService, public auth: AuthService,
    public dialog: MatDialog, private chartservice: ChartsService) { }

  ngOnInit() {
    if (this.auth.isLoggedIn) {
      this.uid = this.auth.getUser().uid;
    }
    this.userinfoservice.classify().subscribe(res => {
      if (res.includes("Fail")) {
        return;
      }
      else {
        this.userinfoservice.addClassified(res, this.uid)
      }
    })
    this.classifiedResults$ = this.userinfoservice.getClassified(this.uid);
    this.classifiedResults$.subscribe(results => {
      if (results.length !== 0) {
        this.flag = true;
        results.sort((a, b) => a.createdAt - b.createdAt);
        let str = results[results.length - 1].classification;
        this.lastResult = parseFloat(str.substr(1).slice(0, -1));
        if (this.lastResult < 0.5) {
          this.message = 'Healthy'
        }
        else if (this.lastResult > 0.5) {
          this.message = 'Diabetes'
        }
      }
    })
    this.info$ = this.userinfoservice.getInfo(this.uid);
    this.info$.subscribe(data => {
      if (data.length !== 0) {
        this.flag = true;
        let dateArray = [];
        let bmiArray = [];
        let bloodPressureArray = [];
        data.sort((a, b) => a.createdAt - b.createdAt);
        this.length = data.length;
        this.dataSource = new MatTableDataSource<any>(data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        data.forEach(el => {
          let date = new Date(el.createdAt.toDate());
          let formatDate = this.pipe.transform(date, 'dd/MM/yyyy')
          dateArray.push(formatDate)
          bmiArray.push(el.bmi.toFixed(3))
          bloodPressureArray.push(el.diastolic);
        })
        this.chartservice.createBloodPressureChat(dateArray, bloodPressureArray);
        this.chartservice.createBmiChat(dateArray, bmiArray);
      }
    })
  }

  delete(key: any) {
    this.infoKey = key;
    this.openDeleteDialog();
  }

  openDeleteDialog(): void {
    const dialogRef = this.dialog.open(DeleteDialog, {
      width: '350px',
      height: '250px',
      panelClass: 'delete-dialog-container',
      disableClose: true,
      data: { key: this.infoKey }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.userinfoservice.deleteInfo(this.uid, result);
      }
    });

  }
  openDialog(data): void {
    const dialogRef = this.dialog.open(EditDialogComponent, {
      width: '400px',
      data: data
    });
  }
}

@Component({
  selector: 'delete-dialog',
  templateUrl: 'delete-dialog.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DeleteDialog {

  constructor(
    public dialogRef: MatDialogRef<DeleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}