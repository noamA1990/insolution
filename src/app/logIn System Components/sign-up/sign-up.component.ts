import { Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  hide = true;
  gender: string;
  firstName: string;
  lastName: string;
  age: number;
  email: string;
  password: string;
  genders = ['male', 'female'];
  genderError = false;
  minDate: Date;
  maxDate: Date;
  birth:Date;


  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  firstNameFormControl = new FormControl('', [
    Validators.required,

  ]);
  lastNameFormControl = new FormControl('', [
    Validators.required,

  ]);
  birthFormControl = new FormControl('', [
    Validators.required,
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(6),
  ]);

  matcher = new MyErrorStateMatcher();

  constructor(private authService: AuthService, private router: Router) {
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 100, 0, 1);
    this.maxDate = new Date(currentYear - 1, 0, 1);
   }

  ngOnInit() { }

  signUp() {
    if (this.gender === undefined) {
      this.genderError = true;
      return;
    }
    else {
      this.genderError = false;
    }
    if (this.birthFormControl.getError('min') || this.birthFormControl.getError('max') ||
      this.emailFormControl.getError('email') || this.passwordFormControl.getError('minLength')) {
      return
    }
    if (this.firstName !== '' && this.lastName !== '' && !this.genderError && this.age !== null
      && this.password !== '' && this.password.length >= 8) {
      this.authService.SignUp(this.email, this.password, this.firstName, this.lastName, this.birth, this.gender);
      // this.router.navigate(['log-in']);
    }
  }
}
